import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MessPopupPageComponent } from './mess-popup-page.component';

describe('MessPopupPageComponent', () => {
  let component: MessPopupPageComponent;
  let fixture: ComponentFixture<MessPopupPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MessPopupPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MessPopupPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
