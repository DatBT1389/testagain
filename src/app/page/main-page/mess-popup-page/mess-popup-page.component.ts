import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-mess-popup-page',
  templateUrl: './mess-popup-page.component.html',
  styleUrls: ['./mess-popup-page.component.css']
})
export class MessPopupPageComponent implements OnInit {

  toggle: boolean = false;
  messForm!: FormGroup;
  confirmMessData = {
    userName: '',
    email: '',
    content: '',
  };
  constructor(private fb: FormBuilder) {
    this.messForm = this.fb.group({
      userName: [, Validators.required],
      email: [, Validators.required],
      content: [, Validators.required],
    });
  }
  ngOnInit(): void {}

  clickMe(): void {
    this.toggle = false;
  }
  submitForm() {
    this.confirmMessData.userName = this.messForm.get('userName')?.value;
    this.confirmMessData.email = this.messForm.get('email')?.value;
    this.confirmMessData.content = this.messForm.get('content')?.value;
    this.toggle = false;
  }

}
