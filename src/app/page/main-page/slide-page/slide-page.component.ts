import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slide-page',
  templateUrl: './slide-page.component.html',
  styleUrls: ['./slide-page.component.css']
})
export class SlidePageComponent implements OnInit {
  value1 = 5;
  value2 = 0.35;
  constructor() { }

  ngOnInit(): void {
  }

}
