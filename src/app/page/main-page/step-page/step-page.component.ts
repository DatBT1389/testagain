import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-step-page',
  templateUrl: './step-page.component.html',
  styleUrls: ['./step-page.component.css']
})
export class StepPageComponent implements OnInit {
index = 0;
  disable = false;
  current = 0;
  indexTemplate:any

  // index = 'First-content';

  // pre(): void {
  //   this.current -= 1;
  //   this.changeContent();
  // }

  // next(): void {
  //   this.current += 1;
  //   this.changeContent();
  // }

  // done(): void {
  //   console.log('done');
  // }

  // changeContent(): void {
  //   switch (this.current) {
  //     case 0: {
  //       this.index = 'First-content';
  //       break;
  //     }
  //     case 1: {
  //       this.index = 'Second-content';
  //       break;
  //     }
  //     case 2: {
  //       this.index = 'third-content';
  //       break;
  //     }
  //     default: {
  //       this.index = 'error';
  //     }
  //   }
  // }

  constructor() { }

  ngOnInit(): void {
  }

  onIndexChange(index: number): void {
    this.index = index;
    console.log(this.index);

    if(this.index == 0) this.indexTemplate ="step1"
  }
}
