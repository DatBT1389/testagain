import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css'],
})
export class MainPageComponent implements OnInit {
  theme = true;
  isCollapsed = false;

  nzOffsetBottom = 10;
  value: any;
  toggle: boolean = false;
  toggleP: boolean = false;
  messForm!: FormGroup;
  confirmMessData = {
    userName: '',
    email: '',
    content: '',
  };
  constructor(private fb: FormBuilder) {
    this.messForm = this.fb.group({
      userName: [, Validators.required],
      email: [, Validators.required],
      content: [, Validators.required],
    });
  }
  ngOnInit(): void {}
  showPopup(){
  }
  clickMe(): void {
    this.toggle = !this.toggle
    window.stop();
  }
  change(value: boolean): void {
    console.log(value);
    window.stop();
  }
  submitForm() {
    this.confirmMessData.userName = this.messForm.get('userName')?.value;
    this.confirmMessData.email = this.messForm.get('email')?.value;
    this.confirmMessData.content = this.messForm.get('content')?.value;
    this.toggle = false;
  }
}
