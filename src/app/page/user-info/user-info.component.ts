import { Component, OnInit } from '@angular/core';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { UserInfoService } from 'src/service/user-info.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {

  total = 1;
  listOfUser: any;
  loading = true;
  pageSize = 10;
  pageIndex = 1;
  tottalUser = 0;
  filterGender = [
    { text: 'male', value: 'male' },
    { text: 'female', value: 'female' }
  ];



  constructor(private userService: UserInfoService) {}

  ngOnInit(): void {
    this.loading = true;
    this.userService.getUserInfo().subscribe(data => {
      this.loading = false;
      this.total = 200; // mock the total data here
      this.listOfUser = data;
      for(let i of this.listOfUser){
        this.tottalUser++
      }
      console.log(this.tottalUser);
    });
  }
}
