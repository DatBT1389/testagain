import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

interface UserInforInterFace {
  id: number,
  first_name: string,
  last_name: string,
  email: string,
  gender: string,
  ip_address: string

}
@Injectable({
  providedIn: 'root'
})
export class UserInfoService {

  constructor( private http: HttpClient) { }
  getUserInfo()
  {
    return this.http.get('http://localhost:3000/infomation');
  }
  updateUserInfo(userId: string, updateData: any) {
    const endPointUrl = 'http://localhost:3000/infomation/' + userId;
    return this.http.put(endPointUrl, updateData);
  }
}
